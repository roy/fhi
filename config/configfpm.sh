#!/bin/bash
# ===============================================
# Script Name: configfpm.sh
# Description: Launcher of fypp and creation of compilation flags
# Author: Marco Mancini
# Version: 1.0
# Date: 2024-07-01
# ===============================================

export FPM_BUILD_DIR='build'

# Creation of F90 files from fypp
TMP_F90_DIR="build/src_temp"
mkdir -p ${TMP_F90_DIR}
for ii in $(ls src/serial/*fpp src/mpi/*fpp); do
    #fypp $ii ${ii%fpp}F90
    fypp $ii ${TMP_F90_DIR}/$(basename "${ii%.*}").F90;
done
/bin/cp -f src/serial/*F90 src/mpi/*F90 ${TMP_F90_DIR}/

# creation of git version
echo "#define GITVERSION \"$(git --version|awk '{print $3}')\"" > ${TMP_F90_DIR}/Version.h


# test
TMP_TEST_DIR="test/temp"
mkdir -p ${TMP_TEST_DIR}
#/bin/cp -f test/*F90 ${TMP_TEST_DIR}/
for ii in $(ls test/*fpp);  do
    fypp $ii ${TMP_TEST_DIR}/$(basename "${ii%.*}").F90;
done

export FPM_FFLAGS=`python3 ./config/config.py`
