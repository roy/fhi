#!/bin/bash
# ===============================================
# Script Name: configfpm.sh
# Description: wrapper to build with fpm and fypp
# Author: Marco Mancini
# Version: 0.0
# Date: 2024-07-19
# ===============================================

set +x

CMD=$*

INSTRUCTION=$1
ARGS="${@:2}"

case $INSTRUCTION in
    "build"|"test")
        source ./config/configfpm.sh
        fpm $INSTRUCTION $ARGS
        ;;

    "clean")
        fpm clean --all
        rm -rf test/temp
        ;;

    *)
        echo " The instruction $INSTRUCTION does not exist or it is not" 
        echo " yet implemented in the script $0"
        echo " ... exiting"
        exit 0 

esac
