#:include "../src/fypp/common_fypp.fpp"
#:set RANKS = range(1, MAXRANK + 1)

module test_attribute
  use testdrive, only : new_unittest, unittest_type, error_type, check
  implicit none
  private

  public :: collect_attribute, get_builddir_var
  
  character(:), public, allocatable :: builddir
  
contains

!> Get build dir if environment variable is defined
  subroutine get_builddir_var()
    character(40) :: var
    integer :: i

    call get_environment_variable('FPM_BUILD_DIR',var)

    if(len(trim(var))==0) then
      builddir = ""
      return
    end if

    do i = len_trim(var), 1, -1
      if (var(i:i) /= ' ') then
          if (var(i:i) == '/') then
            builddir = var(:i)
          else
            builddir = var(:i)//'/'
          end if
          return
        end if
   end do
  end subroutine get_builddir_var

!> Collect all exported unit tests
  subroutine collect_attribute(testsuite)
    !> Collection of tests
    type(unittest_type), allocatable, intent(out) :: testsuite(:)

    testsuite = [ &
      new_unittest("INT32", test_INT32), &
    #:for rank in RANKS
      #:set RName = rname('test',rank, 'INT32')
      new_unittest("${rank}$d_INT32", ${RName}$), &
    #:endfor  
    #:for k1 in REAL_KINDS
      new_unittest("${k1}$", test_${k1}$), &
    #:endfor
    #:for k1 in REAL_KINDS
      #:for rank in RANKS
        #:set RName = rname('test',rank, k1)
      new_unittest("${rank}$d_${k1}$", ${RName}$), &
      #:endfor
    #:endfor
      new_unittest("char", test_char), &
      new_unittest("logical", test_logical) &
    ]

  end subroutine collect_attribute

  subroutine test_INT32(error)
    use fhi_manage_interface_m
    use fhi_manage_files_m
    use fhi_read_attribute_m
    use fhi_write_attribute_m
    use hdf5
    use iso_fortran_env, only : INT32
    type(error_type), allocatable, intent(out) :: error
    integer(HID_T) :: file_id
    integer(INT32) :: attribute
    character(:), allocatable :: filename
    integer :: stat
    
    filename = builddir//'test_INT32.h5'
    open(unit=1234, iostat=stat, file=filename, status='old')
    if (stat == 0) close(1234, status='delete')

    attribute = 1
    print *,attribute
    call fhi_init()
    call fhi_create_file(filename, file_id)
    call fhi_write_attribute(file_id, 'attribute', attribute)
    call fhi_close_file(file_id)
    call fhi_open_file(filename, file_id)
    call fhi_read_attribute(file_id, 'attribute', attribute)
    call fhi_finalize()
    call check(error, attribute == 1) 
    if (allocated(error)) return

  end subroutine test_INT32

#:for rank in RANKS
  #:set RName = rname('test',rank, 'INT32')
  subroutine ${RName}$(error)
    use fhi_manage_interface_m
    use fhi_manage_files_m
    use fhi_read_attribute_m
    use fhi_write_attribute_m
    use hdf5
    use iso_fortran_env, only : INT32
    type(error_type), allocatable, intent(out) :: error
    integer(HID_T) :: file_id
    integer(INT32), allocatable :: attribute${ranksuffix(rank)}$
    integer :: lbounds(${rank}$)
    integer :: ubounds(${rank}$)
    character(:), allocatable :: filename
    integer :: stat

    filename = builddir//'${RName}$.h5'
    open(unit=1234, iostat=stat, file=filename, status='old')
    if (stat == 0) close(1234, status='delete')

    lbounds = 1
    ubounds = 10
    allocate(attribute(${boundssuffix(rank,'lbounds','ubounds')}$))
    attribute = 1
    call fhi_init()
    call fhi_create_file(filename, file_id)
    call fhi_write_attribute(file_id, 'attribute', attribute, ubounds)
    call fhi_close_file(file_id)
    call fhi_open_file(filename, file_id)
    call fhi_read_attribute(file_id, 'attribute', attribute, ubounds)
    call fhi_finalize()
    call check(error, all(attribute == 1))
    if (allocated(error)) return

  end subroutine ${RName}$
#:endfor

#:for k1, t1 in REAL_KINDS_TYPES
  subroutine test_${k1}$(error)
    use fhi_manage_interface_m
    use fhi_manage_files_m
    use fhi_read_attribute_m
    use fhi_write_attribute_m
    use hdf5
    use iso_fortran_env, only : ${k1}$
    type(error_type), allocatable, intent(out) :: error
    integer(HID_T) :: file_id
    ${t1}$ :: attribute
    character(:), allocatable :: filename
    integer :: stat

    filename = builddir//'test_${k1}$.h5'
    open(unit=1234, iostat=stat, file=filename, status='old')
    if (stat == 0) close(1234, status='delete')

    attribute = 1.0_${k1}$
    call fhi_init()
    call fhi_create_file(filename, file_id)
    call fhi_write_attribute(file_id, 'attribute', attribute)
    call fhi_close_file(file_id)
    call fhi_open_file(filename, file_id)
    call fhi_read_attribute(file_id, 'attribute', attribute)
    call fhi_finalize()
    call check(error, attribute == 1.0_${k1}$)
    if (allocated(error)) return

  end subroutine test_${k1}$
#:endfor

#:for k1, t1 in REAL_KINDS_TYPES
  #:for rank in RANKS
  #:set RName = rname('test',rank, k1)
  subroutine ${RName}$(error)
    use fhi_manage_interface_m
    use fhi_manage_files_m
    use fhi_read_attribute_m
    use fhi_write_attribute_m
    use hdf5
    use iso_fortran_env, only : ${k1}$
    type(error_type), allocatable, intent(out) :: error
    integer(HID_T) :: file_id
    ${t1}$, allocatable :: attribute${ranksuffix(rank)}$
    integer :: lbounds(${rank}$)
    integer :: ubounds(${rank}$)
    character(:), allocatable :: filename
    integer :: stat

    filename = builddir//'${RName}$.h5'
    open(unit=1234, iostat=stat, file=filename, status='old')
    if (stat == 0) close(1234, status='delete')

    lbounds = 1
    ubounds = 10
    allocate(attribute(${boundssuffix(rank,'lbounds','ubounds')}$))
    attribute = 1.0_${k1}$
    call fhi_init()
    call fhi_create_file(filename, file_id)
    call fhi_write_attribute(file_id, 'attribute', attribute, ubounds)
    call fhi_close_file(file_id)
    call fhi_open_file(filename, file_id)
    call fhi_read_attribute(file_id, 'attribute', attribute, ubounds)
    call fhi_finalize()
    call check(error, all(attribute == 1.0_${k1}$))
    if (allocated(error)) return

  end subroutine ${RName}$
  #:endfor
#:endfor

  subroutine test_char(error)
    use fhi_manage_interface_m
    use fhi_manage_files_m
    use fhi_read_attribute_m
    use fhi_write_attribute_m
    use hdf5
    type(error_type), allocatable, intent(out) :: error
    integer(HID_T) :: file_id
    character(:), allocatable :: attribute
    character(:), allocatable :: filename
    integer :: stat

    filename = builddir//'test_char.h5'
    open(unit=1234, iostat=stat, file=filename, status='old')
    if (stat == 0) close(1234, status='delete')

    attribute = 'test_char'
    call fhi_init()
    call fhi_create_file(filename, file_id)
    call fhi_write_attribute(file_id, 'attribute', attribute)
    call fhi_close_file(file_id)
    call fhi_open_file(filename, file_id)
    call fhi_read_attribute(file_id, 'attribute', attribute)
    call fhi_finalize()
    call check(error, attribute == 'test_char' )
    if (allocated(error)) return

  end subroutine test_char

  subroutine test_logical(error)
    use fhi_manage_interface_m
    use fhi_manage_files_m
    use fhi_read_attribute_m
    use fhi_write_attribute_m
    use hdf5
    type(error_type), allocatable, intent(out) :: error
    integer(HID_T) :: file_id
    logical :: attribute
    character(:), allocatable :: filename
    integer :: stat

    filename = builddir//'test_logical.h5'
    open(unit=1234, iostat=stat, file=filename, status='old')
    if (stat == 0) close(1234, status='delete')

    attribute = .true.
    call fhi_init()
    call fhi_create_file(filename, file_id)
    call fhi_write_attribute(file_id, 'attribute', attribute)
    call fhi_close_file(file_id)
    call fhi_open_file(filename, file_id)
    call fhi_read_attribute(file_id, 'attribute', attribute)
    call fhi_finalize()
    call check(error, attribute )
    if (allocated(error)) return

  end subroutine test_logical

end module test_attribute