#:include "../src/fypp/common_fypp.fpp"
#:set RANKS = range(1, MAXRANK + 1)
module test_data
  use testdrive, only : new_unittest, unittest_type, error_type, check
  use test_attribute, only : builddir
  implicit none
  private

  public :: collect_data

contains

!> Collect all exported unit tests
subroutine collect_data(testsuite)
  !> Collection of tests
  type(unittest_type), allocatable, intent(out) :: testsuite(:)

  testsuite = [ &
    #:for kind in INT_KINDS
    #:set RName = rname('test', 0, kind)
    new_unittest("0d_${kind}$", ${RName}$), &
      #:for rank in RANKS
      #:set RName = rname('test', rank, kind)
    new_unittest("${rank}$d_${kind}$", ${RName}$), &
      #:endfor  
    #:endfor
    #:for kind in REAL_KINDS
      #:for rank in RANKS
      #:set RName = rname('test', rank, kind)
    new_unittest("${rank}$d_${kind}$", ${RName}$)#{if ((rank<MAXRANK) or (kind != REAL_KINDS[-1] )) }#,#{endif}# &
      #:endfor
    #:endfor
    ]
end subroutine collect_data


#:for kind, type in INT_KINDS_TYPES
  #:set RName = rname('test', 0, kind)
  subroutine ${RName}$(error)

    use fhi_manage_interface_m
    use fhi_manage_files_m
    use fhi_read_data_m
    use fhi_write_data_m
    use hdf5
    use iso_fortran_env, only : ${kind}$
    type(error_type), allocatable, intent(out) :: error
    integer(HID_T) :: file_id
    ${type}$ :: data
    character(:), allocatable :: filename
    integer :: stat

    filename = builddir//'${RName}$.h5'
    open(unit=1234, iostat=stat, file=filename, status='old')
    if (stat == 0) close(1234, status='delete')

    data = 1_${kind}$
    call fhi_init()
    call fhi_create_file(filename, file_id)
    call fhi_write_data(file_id, 'data', data)
    call fhi_close_file(file_id)
    call fhi_open_file(filename, file_id,rw='r')
    call fhi_read_data(file_id, 'data', data)
    call fhi_close_file(file_id)
    call fhi_finalize()
    call check(error, data == 1_${kind}$)
    if (allocated(error)) return
  
  end subroutine ${RName}$

  #:for rank in RANKS
  #:set RName = rname('test',rank, kind)
  subroutine ${RName}$(error)
    use fhi_manage_interface_m
    use fhi_manage_files_m
    use fhi_read_data_m
    use fhi_write_data_m
    use hdf5
    use iso_fortran_env, only : ${kind}$
    type(error_type), allocatable, intent(out) :: error
    integer(HID_T) :: file_id
    ${type}$, allocatable :: data${ranksuffix(rank)}$
    integer :: lbounds(${rank}$)
    integer :: ubounds(${rank}$)
    character(:), allocatable :: filename
    integer :: stat

    filename = builddir//'${RName}$.h5'
    open(unit=1234, iostat=stat, file=filename, status='old')
    if (stat == 0) close(1234, status='delete')

    lbounds = 1
    ubounds = 10
    allocate(data(${boundssuffix(rank,'lbounds','ubounds')}$))
    data = 1_${kind}$
    call fhi_init()
    call fhi_create_file(filename, file_id)
    call fhi_write_data(file_id, 'data', data, ubounds)
    call fhi_close_file(file_id)
    call fhi_open_file(filename, file_id)
    call fhi_read_data(file_id, 'data', data, ubounds)
    call fhi_close_file(file_id)
    call fhi_finalize()
    call check(error, all(data == 1_${kind}$))
    if (allocated(error)) return

  end subroutine ${RName}$
  #:endfor
#:endfor


#:for kind, type in REAL_KINDS_TYPES
  #:for rank in RANKS
  #:set RName = rname('test',rank, kind)
  subroutine ${RName}$(error)
    use fhi_manage_interface_m
    use fhi_manage_files_m
    use fhi_read_data_m
    use fhi_write_data_m
    use hdf5
    use iso_fortran_env, only : ${kind}$
    type(error_type), allocatable, intent(out) :: error
    integer(HID_T) :: file_id
    ${type}$, allocatable :: data${ranksuffix(rank)}$
    integer :: lbounds(${rank}$)
    integer :: ubounds(${rank}$)
    character(:), allocatable :: filename
    integer :: stat

    filename = builddir//'${RName}$.h5'
    open(unit=1234, iostat=stat, file=filename, status='old')
    if (stat == 0) close(1234, status='delete')

    lbounds = 1
    ubounds = 10
    allocate(data(${boundssuffix(rank,'lbounds','ubounds')}$))
    data = 1.0_${kind}$
    call fhi_init()
    call fhi_create_file(filename, file_id)
    call fhi_write_data(file_id, 'data', data, ubounds)
    call fhi_close_file(file_id)
    call fhi_open_file(filename, file_id)
    call fhi_read_data(file_id, 'data', data, ubounds)
    call fhi_close_file(file_id)
    call fhi_finalize()
    call check(error, all(data == 1.0_${kind}$))
    if (allocated(error)) return

  end subroutine ${RName}$
  #:endfor
#:endfor




end module test_data