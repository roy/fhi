# Fortran HDF5 interface

Simplified Fortran interface for HDF5 I/O.
This library provides wrappers that allow to write and read HDF5 files with Fortran.

## Build

### Requirements
- CMake 3.15 or higher: https://cmake.org/
- fypp preprocessor: https://github.com/aradi/fypp

Serial version:
- HDF5 1.8.x built with --enable-fortran2003
- HDF5 1.10.x built with --enable-fortran

Parallel version:
- a MPI distribution
- HDF5 1.8.x built with --enable-parallel --enable-fortran2003
- HDF5 1.10.x build with --enable-parallel --enable-fortran

Tests:
- test-drive: https://github.com/fortran-lang/test-drive

### Build the library

#### With CMake

Default build: from the fhi directory
```
cmake -B build
cd build
make
```
The library is in build/lib, the Fortran module files are in build/mod

If you want to install the library, you may give an install prefix:
```
cmake -DCMAKE_INSTALL_PREFIX=/my/install/prefix -B build
cd build
make
make install
```
The library will be installed in ```/my/install/prefix/lib```.
The Fortran module files will be installed in ```/my/install/prefix/include/CompilerID-CompilerVersion```.
For instance, if you compile the library with gfortran 11.4, the path to the Fortran module files will be ```/my/install/prefix/include/GNU-11.4.0/```.

#### With fpm
The library can be built with the [Fortran Package Manager fpm](https://fpm.fortran-lang.org/index.html).
This method will automatically install test-drive.
You will need to run a shell script before building with fpm.
```
. config/configfpm.sh
fpm build
```

### Use the library

#### With CMake
If you want to use the library for a project that is built with CMake, you can use the ```find_package``` function. You can additionnaly specify that you need the parallel functions or only the serial functions.
You should add the following lines in the CMakeLists.txt file of your project if you need the parallel version, or only the first line for the serial version.
```
find_package( fhi REQUIRED COMPONENTS serial)
find_package( fhi REQUIRED COMPONENTS mpi)
```
Additionnaly you will have to configure your project with the ```-DCMAKE_PREFIX_PATH=/my/fhi/install/prefix``` option, and add the following lines to link you applications with fhi (or only the first line for the serial version)
```
target_link_libraries(${MYTARGET} PRIVATE fhi::fhi-serial)
target_link_libraries(${MYTARGET} PRIVATE fhi::fhi-mpi)
```

#### With fpm
TODO
## Documentation
Documentation can be found [here](https://roy.pages.obspm.fr/fhi/index.html).

### Quick start guide

Example:
```fortran
program mycode

use fhi_constants_m
use fhi_manage_interface_m
use fhi_manage_files_m
use fhi_write_data_m
use hdf5
use iso_fortran_env

implicit none

integer(hid_t) :: file_id
integer(INT32), dimension(3) :: data_integer4
character(H5_FILENAME_LEN) :: file_name
character(:), allocatable :: data_name

call fhi_init()

file_name = 'myresults.h5'
call fhi_create_file(file_name,file_id)

data_name = 'mydata_integer'
data_integer4 = (/1, 2, 3/)
call fhi_write_data(file_id, data_name, data_integer4, [3])

call fhi_close_file(file_id)
call fhi_finalize()

end program mycode
```
