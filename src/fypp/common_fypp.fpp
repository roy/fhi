!This file is a modified version of the common_fypp.fpp file from the fortran-lang/stdlib project.
!  https://github.com/fortran-lang/stdlib

#:mute

#! Real kinds to be considered during templating
#:set REAL_KINDS = ["REAL32", "REAL64"]

#! Real types to be considered during templating
#:set REAL_TYPES = ["real({})".format(k) for k in REAL_KINDS]

#! Collected (kind, type) tuples for real types
#:set REAL_KINDS_TYPES = list(zip(REAL_KINDS, REAL_TYPES))

#! Integer kinds to be considered during templating
#:set INT_KINDS = ["INT32", "INT64"]

#! Integer types to be considered during templating
#:set INT_TYPES = ["integer({})".format(k) for k in INT_KINDS]

#! Collected (kind, type) tuples for integer types
#:set INT_KINDS_TYPES = list(zip(INT_KINDS, INT_TYPES))

#! Ranks to be generated when templates are created
#:set MAXRANK = 3

#! Generates an array rank suffix.
#!
#! Args:
#!     rank (int): Rank of the variable
#!
#! Returns:
#!     Array rank suffix string (e.g. (:,:) if rank = 2)
#!
#:def ranksuffix(rank)
#{if rank > 0}#(${":" + ",:" * (rank - 1)}$)#{endif}#
#:enddef

#! Generates an 1st element array rank suffix.
#!
#! Args:
#!     rank (int): Rank of the variable
#!
#! Returns:
#!     Array rank suffix string (e.g. (1,1) if rank = 2)
#!
#:def firstranksuffix(rank)
#{if rank > 0}#(${"1" + ",1" * (rank - 1)}$)#{endif}#
#:enddef

#! Generates a routine name from a generic name, rank, type and kind
#!
#! Args:
#!   gname (str): Generic name
#!   rank (integer): Rank if exist
#!   kind (str): kind of inputs variable
#!   suffix (str): other identifier (could be used for output type/kind)
#!
#! Returns:
#!   A string with a new name
#!
#:def rname(gname, rank, kind, suffix='')
  $:"{0}_{1}d_{2}".format(gname, rank, kind) if suffix == '' else "{0}_{1}d_{2}_{3}".format(gname, rank, kind, suffix)
#:enddef

#! Generates an allocation (lowerbound:upperbound) suffix 
#!
#! Args:
#!    rank (int): Rank of the variable
#!    lb (str): name of the lowerbound array
#!    ub (str): name of the upperbound array
#!
#! Returns:
#!    Allocation suffix string (e.g. (lb(1):ub(1),lb(2):ub(2)))
#!
#:def boundssuffix(rank,lb,ub)
#{if rank > 0}#${lb}$(1):${ub}$(1)#{for dim in range(2,rank+1)}#,${lb}$(${dim}$):${ub}$(${dim}$)#{endfor}##{endif}#
#:enddef

#:endmute
