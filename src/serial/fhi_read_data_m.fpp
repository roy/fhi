!----------------------------------------------------------------------------------------------------------------------------------
! Copyright 2014 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of fortran-hdf5-interface.
!
! fortran-hdf5-interface is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! fortran-hdf5-interface is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with fortran-hdf5-interface.  If not, see <http://www.gnu.org/licenses/>.
!----------------------------------------------------------------------------------------------------------------------------------
#:include "../fypp/common_fypp.fpp"
#:set RANKS = range(1, MAXRANK + 1)
module fhi_read_data_m
  !! author: Fabrice Roy
  !! HDF5 read wrapper 

  use fhi_constants_m
  use hdf5
  use iso_fortran_env

  implicit none

  private

  public :: fhi_read_data

  interface fhi_read_data
  !! author: Fabrice Roy
  !! Generic inteface used to read data from a hdf5 file.
  #:for k1, t1 in INT_KINDS_TYPES
    #:set RName = rname('fhi_read',0, k1)
    module procedure ${RName}$
  #:endfor
  #:for k1, t1 in INT_KINDS_TYPES
    #:for rank in RANKS
      #:set RName = rname('fhi_read',rank, k1)
    module procedure ${RName}$
    #:endfor
  #:endfor  
  #:for k1, t1 in REAL_KINDS_TYPES
    #:for rank in RANKS
      #:set RName = rname('fhi_read',rank, k1)
    module procedure ${RName}$
    #:endfor
  #:endfor
  end interface fhi_read_data

contains
#:for k1, t1 in INT_KINDS_TYPES
  #:set RName = rname('fhi_read',0, k1)

    !------------------------------------------------------------------------------------------------------------------------------
    subroutine ${RName}$(id, name, data)
      !! author: Fabrice Roy
      !! Read a ${k1}$ from a HDF5 file
      ${t1}$, intent(inout), target :: data
      !! data read from file
      integer(hid_t), intent(in) :: id
      !! hdf5 id of the file or group where data is read from
      character(*), intent(in) :: name
      !! name of the dataset

      integer(hid_t) :: dset_id
      integer(hid_t) :: h5_kind
      integer :: h5err
      type(c_ptr) :: ptr
        
      character(*), parameter :: ROUTINE_NAME = '${RName}$'
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' begins'
#endif

      ptr = c_loc(data)
      ! hdf5 type corresponding to the integer type to read
      h5_kind = h5kind_to_type(${k1}$,H5_INTEGER_KIND)

      ! open the dataset
      call h5dopen_f(id, name, dset_id, h5err)

      ! read the dataset
      call h5dread_f(dset_id, h5_kind, ptr, h5err)

      ! close the dataset
      call h5dclose_f(dset_id, h5err)
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' ends'
#endif
    end subroutine ${RName}$
#:endfor
#:for k1, t1 in INT_KINDS_TYPES
  #:for rank in RANKS
    #:set RName = rname('fhi_read',rank, k1)

    !------------------------------------------------------------------------------------------------------------------------------
    subroutine ${RName}$(id, name, data, ubounds)
      !! author: Fabrice Roy
      !! Read a ${k1}$ dim(${rank}$) array from a HDF5 file
      ${t1}$, intent(inout), target :: data${ranksuffix(rank)}$
      !! data read from file
      integer(hid_t), intent(in) :: id
      !! hdf5 id of the file or group where data is read from
      character(*), intent(in) :: name
      !! name of the dataset
      integer(INT32), intent(in) :: ubounds(${rank}$)
      !! dimensions of the dataset

      integer(hid_t) :: dset_id                               ! id of the dataset
      integer(hid_t) :: h5_kind                               ! HDF5 integer type
      integer :: h5err
      type(c_ptr) :: ptr
        
      character(*), parameter :: ROUTINE_NAME = '${RName}$'
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' begins'
#endif

      ptr = c_loc(data${firstranksuffix(rank)}$)
      ! hdf5 type corresponding to the integer type to read
      h5_kind = h5kind_to_type(${k1}$,H5_INTEGER_KIND)

      ! open the dataset
      call h5dopen_f(id, name, dset_id, h5err)

      ! read the dataset
      call h5dread_f(dset_id, h5_kind, ptr, h5err)

      ! close the dataset
      call h5dclose_f(dset_id, h5err)
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' ends'
#endif
    end subroutine ${RName}$

  #:endfor
#:endfor
#:for k1, t1 in REAL_KINDS_TYPES
  #:for rank in RANKS
    #:set RName = rname('fhi_read',rank, k1)

    !------------------------------------------------------------------------------------------------------------------------------
    subroutine ${RName}$(id, name, data, ubounds)
      !! author: Fabrice Roy
      !! Read a ${k1}$ dim(${rank}$) array from a HDF5 file
      ${t1}$, intent(inout), target :: data${ranksuffix(rank)}$
      !! data read from file
      integer(hid_t), intent(in) :: id
      !! hdf5 id of the file or group where data is read from
      character(*), intent(in) :: name
      !! name of the dataset
      integer(INT32), intent(in) :: ubounds(${rank}$)
      !! dimensions of the dataset

      integer(hid_t) :: dset_id                               ! id of the dataset
      integer(hid_t) :: h5_kind                               ! HDF5 integer type
      integer :: h5err
      type(c_ptr) :: ptr
        
      character(*), parameter :: ROUTINE_NAME = '${RName}$'
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' begins'
#endif

      ptr = c_loc(data${firstranksuffix(rank)}$)
      ! hdf5 type corresponding to the integer type to read
      h5_kind = h5kind_to_type(${k1}$,H5_REAL_KIND)

      ! open the dataset
      call h5dopen_f(id, name, dset_id, h5err)

      ! read the dataset
      call h5dread_f(dset_id, h5_kind, ptr, h5err)

      ! close the dataset
      call h5dclose_f(dset_id, h5err)
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' ends'
#endif
    end subroutine ${RName}$

  #:endfor
#:endfor


end module fhi_read_data_m