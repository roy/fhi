!----------------------------------------------------------------------------------------------------------------------------------
! Copyright 2014 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of fortran-hdf5-interface.
!
! fortran-hdf5-interface is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! fortran-hdf5-interface is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with fortran-hdf5-interface.  If not, see <http://www.gnu.org/licenses/>.
!----------------------------------------------------------------------------------------------------------------------------------
#:include "../fypp/common_fypp.fpp"
#:set RANKS = range(1, MAXRANK + 1)
module fhi_write_data_m
  !! author: Fabrice Roy
  !! HDF5 write wrapper 

  use fhi_constants_m
  use hdf5
  use iso_fortran_env

  implicit none

  private

  public :: fhi_write_data

  interface fhi_write_data
  !! author: Fabrice Roy
  !! Generic inteface used to write data to a hdf5 file.
  #:for k1, t1 in INT_KINDS_TYPES
    #:set RName = rname('fhi_write',0, k1)
    module procedure ${RName}$
  #:endfor
  #:for k1, t1 in REAL_KINDS_TYPES
    #:set RName = rname('fhi_write',0, k1)
    module procedure ${RName}$
  #:endfor
  #:for k1, t1 in INT_KINDS_TYPES
    #:for rank in RANKS
      #:set RName = rname('fhi_write',rank, k1)
    module procedure ${RName}$
    #:endfor
  #:endfor  
  #:for k1, t1 in REAL_KINDS_TYPES
    #:for rank in RANKS
      #:set RName = rname('fhi_write',rank, k1)
    module procedure ${RName}$
    #:endfor
  #:endfor
  end interface fhi_write_data

contains
#:for k1, t1 in INT_KINDS_TYPES
  #:set RName = rname('fhi_write',0, k1)

    !------------------------------------------------------------------------------------------------------------------------------
    subroutine ${RName}$(id, name, data)
      !! author: Fabrice Roy
      !! Write a ${k1}$ to a HDF5 file
      ${t1}$, intent(in), target :: data
      !! data written to file
      integer(hid_t), intent(in) :: id
      !! hdf5 id of the file or group where data is written to
      character(*), intent(in) :: name
      !! name of the dataset

      integer(hid_t) :: dset_id
      integer(hid_t) :: dspace_id
      integer(hid_t) :: h5_kind
      integer :: h5err
      type(c_ptr) :: ptr
      integer(hsize_t) :: ubounds(1)
  
      character(*), parameter :: ROUTINE_NAME = '${RName}$'
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' begins'
#endif

      ubounds = 1
      ptr = c_loc(data)
      h5_kind = h5kind_to_type(${k1}$,H5_INTEGER_KIND)
      call h5screate_simple_f(1, ubounds, dspace_id, h5err)
      call h5dcreate_f(id, name, h5_kind, dspace_id, &
          dset_id, h5err)
      call h5dwrite_f(dset_id, h5_kind, ptr, h5err)
      call h5dclose_f(dset_id, h5err)
      call h5sclose_f(dspace_id, h5err)
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' ends'
#endif
    end subroutine ${RName}$
#:endfor
#:for k1, t1 in REAL_KINDS_TYPES
  #:set RName = rname('fhi_write',0, k1)

    !------------------------------------------------------------------------------------------------------------------------------
    subroutine ${RName}$(id, name, data)
      !! author: Fabrice Roy
      !! Write a ${k1}$ to a HDF5 file
      ${t1}$, intent(in), target :: data
      !! data written to file
      integer(hid_t), intent(in) :: id
      !! hdf5 id of the file or group where data is written to
      character(*), intent(in) :: name
      !! name of the dataset

      integer(hid_t) :: dset_id
      integer(hid_t) :: dspace_id
      integer(hid_t) :: h5_kind
      integer :: h5err
      type(c_ptr) :: ptr
      integer(hsize_t) :: ubounds(1)
        
      character(*), parameter :: ROUTINE_NAME = '${RName}$'
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' begins'
#endif
      
      ubounds = 1
      ptr = c_loc(data)
      h5_kind = h5kind_to_type(${k1}$,H5_INTEGER_KIND)
      call h5screate_simple_f(1, ubounds, dspace_id, h5err)
      call h5dcreate_f(id, name, h5_kind, dspace_id, &
          dset_id, h5err)
      call h5dwrite_f(dset_id, h5_kind, ptr, h5err)
      call h5dclose_f(dset_id, h5err)
      call h5sclose_f(dspace_id, h5err)
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' ends'
#endif
    end subroutine ${RName}$
#:endfor
#:for k1, t1 in INT_KINDS_TYPES
  #:for rank in RANKS
    #:set RName = rname('fhi_write',rank, k1)

    !------------------------------------------------------------------------------------------------------------------------------
    subroutine ${RName}$(id, name, data, ubounds)
      !! author: Fabrice Roy
      !! Write a ${k1}$ dim(${rank}$) array to a HDF5 file
      ${t1}$, intent(in), target :: data${ranksuffix(rank)}$
      !! data written to file
      integer(hid_t), intent(in) :: id
      !! hdf5 id of the file or group where data is written to
      character(*), intent(in) :: name
      !! name of the dataset
      integer(INT32), intent(in) :: ubounds(${rank}$)
      !! dimensions of the dataset

      integer(hid_t) :: dset_id
      integer(hid_t) :: dspace_id
      integer(hid_t) :: h5_kind
      integer :: h5err
      type(c_ptr) :: ptr
        
      character(*), parameter :: ROUTINE_NAME = '${RName}$'
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' begins'
#endif

      ptr = c_loc(data${firstranksuffix(rank)}$)
      h5_kind = h5kind_to_type(${k1}$,H5_INTEGER_KIND)
      call h5screate_simple_f(${rank}$, int(ubounds,kind=hsize_t), dspace_id, h5err)
      call h5dcreate_f(id, name, h5_kind, dspace_id, &
          dset_id, h5err)
      call h5dwrite_f(dset_id, h5_kind, ptr, h5err)
      call h5dclose_f(dset_id, h5err)
      call h5sclose_f(dspace_id, h5err)
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' ends'
#endif
    end subroutine ${RName}$

  #:endfor
#:endfor
#:for k1, t1 in REAL_KINDS_TYPES
  #:for rank in RANKS
    #:set RName = rname('fhi_write',rank, k1)

    !------------------------------------------------------------------------------------------------------------------------------
    subroutine ${RName}$(id, name, data, ubounds)
      !! author: Fabrice Roy
      !! Write a ${k1}$ dim(${rank}$) array to a HDF5 file
      ${t1}$, intent(in), target :: data${ranksuffix(rank)}$
      !! data written to file
      integer(hid_t), intent(in) :: id
      !! hdf5 id of the file or group where data is written to
      character(*), intent(in) :: name
      !! name of the dataset
      integer(INT32), intent(in) :: ubounds(${rank}$)
      !! dimensions of the dataset

      integer(hid_t) :: dset_id      
      integer(hid_t) :: dspace_id
      integer(hid_t) :: h5_kind
      integer :: h5err
      type(c_ptr) :: ptr
        
      character(*), parameter :: ROUTINE_NAME = '${RName}$'
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' begins'
#endif

      ptr = c_loc(data${firstranksuffix(rank)}$)
      h5_kind = h5kind_to_type(${k1}$,H5_REAL_KIND)
      call h5screate_simple_f(${rank}$, int(ubounds,kind=hsize_t), dspace_id, h5err)
      call h5dcreate_f(id, name, h5_kind, dspace_id, &
          dset_id, h5err)
      call h5dwrite_f(dset_id, h5_kind, ptr, h5err)
      call h5dclose_f(dset_id, h5err)
      call h5sclose_f(dspace_id, h5err)

#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' ends'
#endif
    end subroutine ${RName}$

  #:endfor
#:endfor


end module fhi_write_data_m