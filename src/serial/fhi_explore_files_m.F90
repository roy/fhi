!----------------------------------------------------------------------------------------------------------------------------------
! Copyright 2020 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of fortran-hdf5-interface.
! This file is based on an example found in the official HDF5 documentation.
!
! fortran-hdf5-interface is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! fortran-hdf5-interface is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with fortran-hdf5-interface.  If not, see <http://www.gnu.org/licenses/>.
!----------------------------------------------------------------------------------------------------------------------------------

module fhi_explore_files_m
  !! author: Fabrice Roy
  !! HDF5 file query routines

  use fhi_constants_m, only : H5_STR_LEN, H5_FULL_PATH_LEN
  use hdf5
  use iso_c_binding
  use iso_fortran_env

  implicit none

  private
  public :: fhi_search

  type :: search_element_t
    character(len=H5_STR_LEN) :: name
    integer :: h5_type
  end type search_element_t

contains

!----------------------------------------------------------------------------------------------------------------------------------
  integer function search_element_function(loc_id, name, info, data_ptr) result (found_element) bind(C)
    !! author: Fabrice Roy
    !! Search for an element in a
    integer(HID_T), intent(in), value :: loc_id
    character(LEN=1), dimension(1:H5_FULL_PATH_LEN), intent(inout) :: name
    type(H5O_info_t), intent(inout) :: info
    type(C_PTR), intent(in), value :: data_ptr

    character(LEN=H5_FULL_PATH_LEN) :: name_string
    integer :: i

    type(search_element_t), pointer :: data_function
    character(*), parameter :: ROUTINE_NAME = 'search_element_function'

#ifdef DEBUGHDF5
    write(ERROR_UNIT,'(a)') ROUTINE_NAME//' begins for dataset ', trim(data_function%name)
#endif

    name_string = ' '
    call c_f_pointer(data_ptr, data_function)
    do i = 1, H5_FULL_PATH_LEN
      if(name(i)(1:1) == C_NULL_CHAR) exit ! Read up to the C NULL termination
      name_string(i:i) = name(i)(1:1)
    end do

    if(trim(name_string) == trim(data_function%name)) then
      if(info%type == data_function%h5_type) then
        found_element = 1 ! return successful
      else
        found_element = 0
      end if
    else
      found_element = 0
    end if

#ifdef DEBUGHDF5
    write(ERROR_UNIT,'(a)') ROUTINE_NAME//' for dataset ', trim(data_function%name)
#endif

  end function search_element_function

!----------------------------------------------------------------------------------------------------------------------------------
  logical function fhi_search(h5_id, search_name, search_type) result(does_exist)
    !! author: Fabrice Roy
    !! Search for an element from its name in a HDF5 file or group, returns .true. if the element is found

    integer(HID_T), intent(in) :: h5_id
    character(len=H5_STR_LEN), intent(in) :: search_name
    integer, intent(in) :: search_type

    type(C_PTR) :: data_function_ptr
    type(C_FUNPTR) :: function_ptr
    integer :: function_ret_value
    integer :: h5err
    type(search_element_t), target :: data_function
    character(*), parameter :: ROUTINE_NAME = 'fhi_search'


#ifdef DEBUGHDF5
    write(ERROR_UNIT,'(a)') ROUTINE_NAME//' begins for dataset ', trim(search_name)
#endif

    data_function%name = search_name
    data_function%h5_type = search_type

    data_function_ptr = c_loc(data_function)
    function_ptr = c_funloc(search_element_function)

    call h5ovisit_f(h5_id, H5_INDEX_NAME_F, H5_ITER_NATIVE_F, function_ptr, data_function_ptr, function_ret_value, h5err)

    does_exist = (function_ret_value == 1)

#ifdef DEBUGHDF5
    write(ERROR_UNIT,'(a)') ROUTINE_NAME//' ends for dataset ', trim(search_name)
#endif

  end function fhi_search

end module fhi_explore_files_m


