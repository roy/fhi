!----------------------------------------------------------------------------------------------------------------------------------
! Copyright 2014 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of fortran-hdf5-interface.
!
! fortran-hdf5-interface is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! fortran-hdf5-interface is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with fortran-hdf5-interface.  If not, see <http://www.gnu.org/licenses/>.
!----------------------------------------------------------------------------------------------------------------------------------

#:include "../fypp/common_fypp.fpp"
#:set RANKS = range(1, MAXRANK + 1)
module fhi_read_attribute_m
  !! author: Fabrice Roy
  !! HDF5 read attribute wrapper 

  use fhi_constants_m
  use hdf5
  use iso_fortran_env

  implicit none

  private

  public :: fhi_read_attribute

  interface fhi_read_attribute
  !! author: Fabrice Roy
  !! Generic inteface used to read attribute from a hdf5 file.
    #:set RName = rname('fhi_read_attr',0, 'char')
    module procedure ${RName}$
    #:set RName = rname('fhi_read_attr',0, 'logical')
    module procedure ${RName}$
    #:set RName = rname('fhi_read_attr',0, 'INT32')
    module procedure ${RName}$
    #:for rank in RANKS
      #:set RName = rname('fhi_read_attr',rank, 'INT32')
    module procedure ${RName}$
    #:endfor
  #:for k1 in REAL_KINDS
    #:set RName = rname('fhi_read_attr',0, k1)
    module procedure ${RName}$
  #:endfor
  #:for k1 in REAL_KINDS
    #:for rank in RANKS
      #:set RName = rname('fhi_read_attr',rank, k1)
    module procedure ${RName}$
    #:endfor
  #:endfor
  end interface fhi_read_attribute

contains
!----------------------------------------------------------------------------------------------------------------------------------
#:set RName = rname('fhi_read_attr',0, 'char')
  subroutine ${RName}$(id, name, attr)
    !! author: Fabrice Roy
    !! Read a character attribute from a HDF5 file
    character(*), intent(inout) :: attr
    !! attribute read from file
    integer(hid_t), intent(in) :: id
    !! hdf5 id of the file or group where data is read from
    character(*), intent(in) :: name
    !! name of the attribute

    integer(hid_t) :: attr_id
    integer(hid_t) :: atype_id
    integer(hsize_t) :: dims(1)
    integer :: h5err

#ifdef DEBUGHDF5
    write(ERROR_UNIT,'(a)') '${RName}$ begins'
#endif

    dims = int(len(attr), kind=hsize_t)
    call h5aopen_name_f(id, name, attr_id, h5err)
    if(h5err/=0) then
      write(ERROR_UNIT,*) 'Attribute ',name,'not found in the hdf5 file.'
      return
    end if
    call h5aget_type_f(attr_id, atype_id, h5err)
    call h5aread_f(attr_id, atype_id, attr, dims, h5err)
    call h5aclose_f(attr_id, h5err)

#ifdef DEBUGHDF5
    write(ERROR_UNIT,'(a)') '${RName}$ ends'
#endif
    end subroutine ${RName}$
#:set RName = rname('fhi_read_attr',0, 'logical')

!----------------------------------------------------------------------------------------------------------------------------------
    subroutine ${RName}$(id, name, attr)
    !! author: Fabrice Roy
    !! Read a logical attribute from a HDF5 file
      logical, intent(inout) :: attr
      !! attribute read from file
      integer(hid_t), intent(in) :: id
      !! hdf5 id of the file or group where data is read from
      character(*), intent(in) :: name
      !! name of the attribute

      integer(hid_t) :: attr_id
      integer(hid_t) :: atype_id
      integer(hsize_t), dimension(1) :: dims
      integer :: h5err
      integer(INT32) :: tmpint                                  
      !! attribute = 0 for .false. and = 1 for .true.
        
      character(*), parameter :: ROUTINE_NAME = '${RName}$'

#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' begins'
#endif

      dims(1) = 1

      call h5aopen_name_f(id, name, attr_id, h5err)
      if(h5err/=0) then
        write(ERROR_UNIT,*) 'Attribute ',name,'not found in the hdf5 file.'
        return
      end if
      call h5aget_type_f(attr_id, atype_id, h5err)
      call h5aread_f(attr_id, atype_id, tmpint, dims, h5err)
      call h5aclose_f(attr_id, h5err)

      attr = (tmpint == 1) 

#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' ends'
#endif
  end subroutine ${RName}$
  #:set RName = rname('fhi_read_attr',0, 'INT32')

    !------------------------------------------------------------------------------------------------------------------------------
    subroutine ${RName}$(id, name, attr)
      !! author: Fabrice Roy
      !! Read a INT32 attribute from a HDF5 file
      integer(INT32), intent(inout) :: attr
      !! attribute read from file
      integer(hid_t), intent(in) :: id
      !! hdf5 id of the file or group where data is read from
      character(*), intent(in) :: name
      !! name of the attribute

      integer(hid_t) :: attr_id
      integer(hid_t) :: atype_id
      integer(hsize_t), dimension(1) :: dims
      integer :: h5err
        
      character(*), parameter :: ROUTINE_NAME = '${RName}$'
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' begins'
#endif

      dims(1) = 1

      call h5aopen_name_f(id, name, attr_id, h5err)
      if(h5err/=0) then
         write(ERROR_UNIT,*) 'Attribute ',name,'not found in the hdf5 file.'
         return
      end if
      call h5aget_type_f(attr_id, atype_id, h5err)
      call h5aread_f(attr_id, atype_id, attr, dims, h5err)
      call h5aclose_f(attr_id, h5err)
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' ends'
#endif
    end subroutine ${RName}$
#:for k1, t1 in REAL_KINDS_TYPES
  #:set RName = rname('fhi_read_attr',0, k1)

    !------------------------------------------------------------------------------------------------------------------------------
    subroutine ${RName}$(id, name, attr)
      !! author: Fabrice Roy
      !! Read a ${k1}$ attribute from a HDF5 file
      ${t1}$, intent(inout) :: attr
      !! attribute read from file
      integer(hid_t), intent(in) :: id
      !! hdf5 id of the file or group where data is read from
      character(*), intent(in) :: name
      !! name of the attribute

      integer(hid_t) :: attr_id
      integer(hid_t) :: atype_id
      integer(hsize_t), dimension(1) :: dims
      integer :: h5err
        
      character(*), parameter :: ROUTINE_NAME = '${RName}$'
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' begins'
#endif

      dims(1) = 1

      call h5aopen_name_f(id, name, attr_id, h5err)
      if(h5err/=0) then
         write(ERROR_UNIT,*) 'Attribute ',name,'not found in the hdf5 file.'
         return
      end if
      call h5aget_type_f(attr_id, atype_id, h5err)
      call h5aread_f(attr_id, atype_id, attr, dims, h5err)
      call h5aclose_f(attr_id, h5err)
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' ends'
#endif
    end subroutine ${RName}$
#:endfor
  #:for rank in RANKS
    #:set RName = rname('fhi_read_attr',rank, 'INT32')

    !------------------------------------------------------------------------------------------------------------------------------
    subroutine ${RName}$(id, name, attr, ubounds)
      !! author: Fabrice Roy
      !! Read a INT32 dim(${rank}$) attribute from a HDF5 file
      integer(INT32), intent(inout) :: attr${ranksuffix(rank)}$
      !! attribute read from file
      integer(hid_t), intent(in) :: id
      !! hdf5 id of the file or group where data is read from
      character(*), intent(in) :: name
      !! name of the attribute
      integer, intent(in) :: ubounds(${rank}$)
      !! dimensions of the attribute

      integer(hid_t) :: attr_id
      integer(hid_t) :: atype_id
      integer(hsize_t) :: dims(${rank}$)
      integer :: h5err
        
      character(*), parameter :: ROUTINE_NAME = '${RName}$'
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' begins'
#endif

      dims = int(ubounds, kind=hsize_t)
      call h5aopen_name_f(id, name, attr_id, h5err)
      if(h5err/=0) then
         write(ERROR_UNIT,*) 'Attribute ',name,'not found in the hdf5 file.'
         return
      end if
      call h5aget_type_f(attr_id, atype_id, h5err)
      call h5aread_f(attr_id, atype_id, attr, dims, h5err)
      call h5aclose_f(attr_id, h5err)
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' ends'
#endif
    end subroutine ${RName}$
  #:endfor
#:for k1, t1 in REAL_KINDS_TYPES
  #:for rank in RANKS
    #:set RName = rname('fhi_read_attr',rank, k1)

    !------------------------------------------------------------------------------------------------------------------------------
    subroutine ${RName}$(id, name, attr, ubounds)
      !! author: Fabrice Roy
      !! Read a ${k1}$ dim(${rank}$) attribute from a HDF5 file
      ${t1}$, intent(inout) :: attr${ranksuffix(rank)}$
      !! attribute read from file
      integer(hid_t), intent(in) :: id
      !! hdf5 id of the file or group where data is read from
      character(*), intent(in) :: name
      !! name of the attribute
      integer, intent(in) :: ubounds(${rank}$)
      !! dimensions of the attribute

      integer(hid_t) :: attr_id
      integer(hid_t) :: atype_id
      integer(hsize_t) :: dims(${rank}$)
      integer :: h5err
        
      character(*), parameter :: ROUTINE_NAME = '${RName}$'
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' begins'
#endif
    
      dims = int(ubounds, kind=hsize_t)
      call h5aopen_name_f(id, name, attr_id, h5err)
      if(h5err/=0) then
         write(ERROR_UNIT,*) 'Attribute ',name,'not found in the hdf5 file.'
         return
      end if
      call h5aget_type_f(attr_id, atype_id, h5err)
      call h5aread_f(attr_id, atype_id, attr, dims, h5err)
      call h5aclose_f(attr_id, h5err)
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' ends'
#endif
    end subroutine ${RName}$
  #:endfor
#:endfor

end module fhi_read_attribute_m