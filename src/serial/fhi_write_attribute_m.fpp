!----------------------------------------------------------------------------------------------------------------------------------
! Copyright 2014 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of fortran-hdf5-interface.
!
! fortran-hdf5-interface is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! fortran-hdf5-interface is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with fortran-hdf5-interface.  If not, see <http://www.gnu.org/licenses/>.
!----------------------------------------------------------------------------------------------------------------------------------

#:include "../fypp/common_fypp.fpp"
#:set RANKS = range(1, MAXRANK + 1)
module fhi_write_attribute_m
  !! author: Fabrice Roy
  !! HDF5 write attribute wrapper 

  use fhi_constants_m
  use hdf5
  use iso_fortran_env

  implicit none

  private

  public :: fhi_write_attribute

  interface fhi_write_attribute
  !! author: Fabrice Roy
  !! Generic inteface used to write attribute to a hdf5 file.
  #:set RName = rname('fhi_write_attr',0, 'char')
    module procedure ${RName}$
  #:set RName = rname('fhi_write_attr',0, 'logical')
    module procedure ${RName}$
    #:set RName = rname('fhi_write_attr',0, 'INT32')
    module procedure ${RName}$
  #:for k1 in REAL_KINDS
    #:set RName = rname('fhi_write_attr',0, k1)
    module procedure ${RName}$
  #:endfor
    #:for rank in RANKS
      #:set RName = rname('fhi_write_attr',rank, 'INT32')
    module procedure ${RName}$
    #:endfor
  #:for k1 in REAL_KINDS
    #:for rank in RANKS
      #:set RName = rname('fhi_write_attr',rank, k1)
    module procedure ${RName}$
    #:endfor
  #:endfor
  end interface fhi_write_attribute

contains
#:set RName = rname('fhi_write_attr',0, 'char')
  subroutine ${RName}$(id, name, attr)
      !! author: Fabrice Roy
      !! Write a character(:) attribute to a HDF5 file
      integer(hid_t), intent(in) :: id
      !! id of the file/group where the attribute will be written
      character(*), intent(in) :: name
      !! name of the attribute
      character(*), intent(in) :: attr
      !! attribute value
      
      integer(size_t) :: alen
      integer(hid_t)  :: aspace_id
      integer(hid_t)  :: attr_id
      integer(hid_t)  :: atype_id
      integer(hsize_t), dimension(1) :: dims
      integer :: h5err
  
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') '${RName}$ begins'
#endif
  
      dims(1) = 1
      alen = len(attr)
      call h5tcopy_f(H5T_NATIVE_CHARACTER, atype_id, h5err)
      call h5tset_size_f(atype_id, alen, h5err)
      call h5screate_f(H5S_SCALAR_F, aspace_id, h5err)
      call h5acreate_f(id, name, atype_id, aspace_id, attr_id, h5err)
      call h5awrite_f(attr_id, atype_id, attr, dims, h5err)
      call h5aclose_f(attr_id, h5err)
      call h5sclose_f(aspace_id, h5err)
      call h5tclose_f(atype_id, h5err)
  
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') '${RName}$ ends'
#endif
    end subroutine ${RName}$

    !------------------------------------------------------------------------------------------------------------------------------
    #:set RName = rname('fhi_write_attr',0, 'logical')
    subroutine ${RName}$(id, name, attr)
      !! author: Fabrice Roy
      !! Write a logical attribute to a HDF5 file
      integer(hid_t), intent(in) :: id
      !! id of the file/group where the attribute will be written
      character(*), intent(in) :: name
      !! name of the attribute
      logical, intent(in) :: attr
      !! attribute value: attribute = 0 for .false. and = 1 for .true.
        
      integer(size_t) :: alen
      integer(hid_t)  :: aspace_id
      integer(hid_t)  :: attr_id
      integer(hid_t)  :: atype_id
      integer(hsize_t), dimension(1) :: dims
      integer :: h5err
      integer :: rank
      integer(INT32) :: tmpint                                  
  
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') '${RName}$ begins'
#endif

        tmpint = 0
        if(attr) tmpint = 1
        dims(1) = 1
        rank = 1
        call h5screate_simple_f(rank, dims, aspace_id, h5err)
        alen = INT32
        call h5tcopy_f(H5T_NATIVE_INTEGER, atype_id, h5err)
        call h5tset_size_f(atype_id, alen, h5err)
        call h5acreate_f(id, name, atype_id, aspace_id, attr_id, h5err)
        call h5awrite_f(attr_id, atype_id, tmpint, dims, h5err)
        call h5aclose_f(attr_id, h5err)
        call h5sclose_f(aspace_id, h5err)
        call h5tclose_f(atype_id, h5err)
  
#ifdef DEBUGHDF5
        write(ERROR_UNIT,'(a)') '${RName}$ ends'
#endif
      end subroutine ${RName}$
  #:set RName = rname('fhi_write_attr',0, 'INT32')

    !------------------------------------------------------------------------------------------------------------------------------
    subroutine ${RName}$(id, name, attr)
      !! author: Fabrice Roy
      !! Write a INT32 attribute to a HDF5 file
      integer(INT32), intent(in) :: attr
      !! attribute written to file
      integer(hid_t), intent(in) :: id
      !! hdf5 id of the file or group where data is written to
      character(*), intent(in) :: name
      !! name of the attribute

      integer(size_t) :: alen
      integer(hid_t)  :: aspace_id
      integer(hid_t) :: attr_id
      integer(hid_t) :: atype_id
      integer(hsize_t) :: dims(1)
      integer :: h5err
      integer :: rank
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') '${RName}$ begins'
#endif

      dims(1) = 1
      rank = 1
      call h5screate_simple_f(rank, dims, aspace_id, h5err)
      alen = INT32
      call h5tcopy_f(H5T_NATIVE_INTEGER, atype_id, h5err)
      call h5tset_size_f(atype_id, alen, h5err)
      call h5acreate_f(id, name, atype_id, aspace_id, attr_id, h5err)
      call h5awrite_f(attr_id, atype_id, attr, dims, h5err)
      call h5aclose_f(attr_id, h5err)
      call h5sclose_f(aspace_id, h5err)
      call h5tclose_f(atype_id, h5err)
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') '${RName}$ begins'
#endif
    end subroutine ${RName}$
#:for k1, t1 in REAL_KINDS_TYPES
  #:set RName = rname('fhi_write_attr',0, k1)

    !------------------------------------------------------------------------------------------------------------------------------
    subroutine ${RName}$(id, name, attr)
      !! author: Fabrice Roy
      !! Write a ${k1}$ attribute to a HDF5 file
      ${t1}$, intent(in) :: attr
      !! attribute written to file
      integer(hid_t), intent(in) :: id
      !! hdf5 id of the file or group where data is written to
      character(*), intent(in) :: name
      !! name of the attribute

      integer(size_t) :: alen
      integer(hid_t)  :: aspace_id
      integer(hid_t) :: attr_id
      integer(hid_t) :: atype_id
      integer(hsize_t) :: dims(1)
      integer :: h5err
      integer :: rank
      character(*), parameter :: ROUTINE_NAME = '${RName}$'
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' begins'
#endif

      dims(1) = 1
      rank = 1
      call h5screate_simple_f(rank, dims, aspace_id, h5err)
      alen = ${k1}$
      #:if k1 == 'REAL32'
      call h5tcopy_f(H5T_NATIVE_REAL, atype_id, h5err)
      #:else
      call h5tcopy_f(H5T_NATIVE_DOUBLE, atype_id, h5err)
      #:endif
      call h5tset_size_f(atype_id, alen, h5err)
      call h5acreate_f(id, name, atype_id, aspace_id, attr_id, h5err)
      call h5awrite_f(attr_id, atype_id, attr, dims, h5err)
      call h5aclose_f(attr_id, h5err)
      call h5sclose_f(aspace_id, h5err)
      call h5tclose_f(atype_id, h5err)
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' ends'
#endif
    end subroutine ${RName}$
#:endfor
  #:for rank in RANKS
    #:set RName = rname('fhi_write_attr',rank, 'INT32')

    !------------------------------------------------------------------------------------------------------------------------------
    subroutine ${RName}$(id, name, attr, ubounds)
      !! author: Fabrice Roy
      !! Write a INT32 dim(${rank}$) attribute to a HDF5 file
      integer(INT32), intent(in) :: attr${ranksuffix(rank)}$
      !! attribute written to file
      integer(hid_t), intent(in) :: id
      !! hdf5 id of the file or group where data is written to
      character(*), intent(in) :: name
      !! name of the attribute
      integer, intent(in) :: ubounds(${rank}$)
      !! dimensions of the attribute

      integer(size_t) :: alen
      integer(hid_t)  :: aspace_id
      integer(hid_t) :: attr_id
      integer(hid_t) :: atype_id
      integer(hsize_t) :: dims(${rank}$)
      integer :: h5err
      integer :: rank
      character(*), parameter :: ROUTINE_NAME = '${RName}$'
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' begins'
#endif

      dims = int(ubounds, kind=hsize_t)
      rank = ${rank}$
      call h5screate_simple_f(rank, dims, aspace_id, h5err)
      alen = INT32
      call h5tcopy_f(H5T_NATIVE_INTEGER, atype_id, h5err)
      call h5tset_size_f(atype_id, alen, h5err)
      call h5acreate_f(id, name, atype_id, aspace_id, attr_id, h5err)
      call h5awrite_f(attr_id, atype_id, attr, dims, h5err)
      call h5aclose_f(attr_id, h5err)
      call h5sclose_f(aspace_id, h5err)
      call h5tclose_f(atype_id, h5err)        

#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' ends'
#endif
    end subroutine ${RName}$
#:endfor
#:for k1, t1 in REAL_KINDS_TYPES
  #:for rank in RANKS
    #:set RName = rname('fhi_write_attr',rank, k1)

    !------------------------------------------------------------------------------------------------------------------------------
    subroutine ${RName}$(id, name, attr, ubounds)
      !! author: Fabrice Roy
      !! Write a ${k1}$ dim(${rank}$) attribute to a HDF5 file
      ${t1}$, intent(in) :: attr${ranksuffix(rank)}$
      !! attribute written to file
      integer(hid_t), intent(in) :: id
      !! hdf5 id of the file or group where data is written to
      character(*), intent(in) :: name
      !! name of the attribute
      integer, intent(in) :: ubounds(${rank}$)
      !! dimensions of the attribute

      integer(size_t) :: alen
      integer(hid_t)  :: aspace_id
      integer(hid_t) :: attr_id
      integer(hid_t) :: atype_id
      integer(hsize_t) :: dims(${rank}$)
      integer :: h5err
      integer :: rank
      character(*), parameter :: ROUTINE_NAME = '${RName}$'
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' begins'
#endif

      dims = int(ubounds, kind=hsize_t)
      rank = ${rank}$
      call h5screate_simple_f(rank, dims, aspace_id, h5err)     
      alen = ${k1}$
      #:if k1 == 'REAL32'
      call h5tcopy_f(H5T_NATIVE_REAL, atype_id, h5err)
      #:else
      call h5tcopy_f(H5T_NATIVE_DOUBLE, atype_id, h5err)
      #:endif
      call h5tset_size_f(atype_id, alen, h5err)
      call h5acreate_f(id, name, atype_id, aspace_id, attr_id, h5err)
      call h5awrite_f(attr_id, atype_id, attr, dims, h5err)
      call h5aclose_f(attr_id, h5err)
      call h5sclose_f(aspace_id, h5err)
      call h5tclose_f(atype_id, h5err)
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a)') ROUTINE_NAME//' ends'
#endif
    end subroutine ${RName}$
	#:endfor
#:endfor

end module fhi_write_attribute_m