!----------------------------------------------------------------------------------------------------------------------------------
! Copyright 2014 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of fortran-hdf5-interface.
!
! fortran-hdf5-interface is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! fortran-hdf5-interface is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with fortran-hdf5-interface.  If not, see <http://www.gnu.org/licenses/>.
!----------------------------------------------------------------------------------------------------------------------------------
#:include "../fypp/common_fypp.fpp"
#:set RANKS = range(1, MAXRANK + 1)
module fhi_write_mpi_data_m
  !! author: Fabrice Roy
  !! HDF5 parallel write wrapper 

  use fhi_constants_m
  use hdf5
  use iso_fortran_env
  use mpi

  implicit none

  private

  public :: fhi_write_mpi_data

  interface fhi_write_mpi_data
  !! author: Fabrice Roy
  !! Generic inteface used to write data to a hdf5 file with MPIIO support.
  #:for k1, t1 in INT_KINDS_TYPES
    #:for rank in RANKS
      #:set RName = rname('fhi_write_mpi',rank, k1)
    module procedure ${RName}$
    #:endfor
  #:endfor  
  #:for k1, t1 in REAL_KINDS_TYPES
    #:for rank in RANKS
      #:set RName = rname('fhi_write_mpi',rank, k1)
    module procedure ${RName}$
    #:endfor
  #:endfor
  end interface fhi_write_mpi_data

  integer, parameter :: ERR_MSG_LEN=256
contains
#:for k1, t1 in INT_KINDS_TYPES
  #:for rank in RANKS
    #:set RName = rname('fhi_write_mpi',rank, k1)

    !------------------------------------------------------------------------------------------------------------------------------
    subroutine ${RName}$(id, name, data, ubounds, comm, empty, indy)
      !! author: Fabrice Roy
      !! Write a ${k1}$ dim(${rank}$) array to a HDF5 file
      ${t1}$, intent(inout), target :: data${ranksuffix(rank)}$
      !! data written to file
      integer(hid_t), intent(in) :: id
      !! hdf5 id of the file or group where data is written to
      character(*), intent(in) :: name
      !! name of the dataset
      integer(INT32), intent(in) :: ubounds(${rank}$)
      !! dimensions of the dataset
      integer, intent(in) :: comm 
      !! communicator used for the MPIIO write
      logical, intent(in), optional :: empty
      !! true if the data is empty and should have a size=0
      logical, intent(in), optional :: indy
      !! ?

      integer :: allocstat
      integer(hid_t) :: dset_id
      integer(hid_t) :: dspace_id
      character(ERR_MSG_LEN) :: error_message
      integer(hsize_t), dimension(${rank}$) :: global_dims
      integer(hid_t) :: h5_kind
      integer :: h5err
      integer(kind=4) :: idproc, iproc, nproc
      integer(hsize_t), dimension(${rank}$) :: local_dims
      integer(hid_t) :: mem_id
      integer :: mpierr
      integer(kind=4), dimension(:), allocatable :: ntab
      integer(hsize_t), dimension(${rank}$) :: offset
      integer(hid_t) :: plist_id
      type(c_ptr) :: ptr
        
      call mpi_comm_size(comm, nproc, mpierr)
      call mpi_comm_rank(comm, idproc, mpierr)

#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a,i0)') '${RName}$ begins on process ', idproc
#endif

      ptr = c_loc(data${firstranksuffix(rank)}$)
      allocate(ntab(nproc), stat=allocstat, errmsg=error_message)
#ifdef DEBUGHDF5
      if(allocstat > 0) then
        write(ERROR_UNIT, '(a)') 'Allocate failed for ntab in ${RName}$'
        write(ERROR_UNIT, '(a)') error_message
        call mpi_abort(MPI_COMM_WORLD, allocstat, mpierr)
      end if
#endif
      
      h5_kind = h5kind_to_type(${k1}$,H5_INTEGER_KIND)
      local_dims = int(ubounds,kind=hsize_t)
      if(present(empty) ) then
        if(empty) then
          local_dims(${rank}$) = 0
        end if
      end if
 
      call mpi_allgather(local_dims(${rank}$), 1, MPI_INTEGER, ntab, 1, MPI_INTEGER, comm, mpierr)

      global_dims = int(ubounds,kind=hsize_t)
      global_dims(${rank}$) = 0
  
      do iproc = 1, nproc
        global_dims(${rank}$) = global_dims(${rank}$) + ntab(iproc)
      end do
  
      if(global_dims(${rank}$) /= 0) then
        offset = 0
  
        do iproc = 1, idproc
          offset(${rank}$) = offset(${rank}$) + ntab(iproc)
        end do
  
        call h5screate_simple_f(${rank}$, global_dims, dspace_id, h5err)
        call h5dcreate_f(id, name, h5_kind, dspace_id, dset_id, h5err)
        call h5screate_simple_f(${rank}$, local_dims, mem_id, h5err)  
        call h5dget_space_f(dset_id, dspace_id, h5err)
        call h5sselect_hyperslab_f(dspace_id, H5S_SELECT_SET_F, offset, local_dims, h5err)
        call h5pcreate_f(H5P_DATASET_XFER_F, plist_id, h5err)
        if(present(indy)) then
          if(indy) then
            call h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_INDEPENDENT_F, h5err)
          else
            call h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, h5err)
          end if
        else
          call h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, h5err)
        end if
        call h5dwrite_f(dset_id, h5_kind, ptr, h5err, file_space_id = dspace_id, mem_space_id = mem_id, xfer_prp = plist_id)
        call h5dclose_f(dset_id, h5err)
        call h5sclose_f(dspace_id, h5err)
        call h5pclose_f(plist_id, h5err)
        call h5sclose_f(mem_id, h5err)
      end if
      deallocate(ntab)
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a,i0)') '${RName}$ ends on process ', idproc
#endif
    end subroutine ${RName}$

  #:endfor
#:endfor
#:for k1, t1 in REAL_KINDS_TYPES
  #:for rank in RANKS
    #:set RName = rname('fhi_write_mpi',rank, k1)

    !------------------------------------------------------------------------------------------------------------------------------
    subroutine ${RName}$(id, name, data, ubounds, comm, empty, indy)
      !! author: Fabrice Roy
      !! Write a ${k1}$ dim(${rank}$) array to a HDF5 file
      ${t1}$, intent(inout), target :: data${ranksuffix(rank)}$
      !! data written to file
      integer(hid_t), intent(in) :: id
      !! hdf5 id of the file or group where data is written to
      character(*), intent(in) :: name
      !! name of the dataset
      integer(INT32), intent(in) :: ubounds(${rank}$)
      !! dimensions of the dataset
      integer, intent(in) :: comm 
      !! communicator used for the MPIIO write
      logical, intent(in), optional :: empty
      !! true if the data is empty and should have a size=0
      logical, intent(in), optional :: indy
      !! ?

      integer :: allocstat
      integer(hid_t) :: dset_id
      integer(hid_t) :: dspace_id
      character(ERR_MSG_LEN) :: error_message
      integer(hsize_t), dimension(${rank}$) :: global_dims
      integer(hid_t) :: h5_kind
      integer :: h5err
      integer(kind=4) :: idproc, iproc, nproc
      integer(hsize_t), dimension(${rank}$) :: local_dims
      integer(hid_t) :: mem_id
      integer :: mpierr
      integer(kind=4), dimension(:), allocatable :: ntab
      integer(hsize_t), dimension(${rank}$) :: offset
      integer(hid_t) :: plist_id
      type(c_ptr) :: ptr

      call mpi_comm_size(comm, nproc, mpierr)
      call mpi_comm_rank(comm, idproc, mpierr)

#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a,i0)') '${RName}$ begins on process ', idproc
#endif

      ptr = c_loc(data${firstranksuffix(rank)}$)
      allocate(ntab(nproc), stat=allocstat, errmsg=error_message)
#ifdef DEBUGHDF5
      if(allocstat > 0) then
        write(ERROR_UNIT, '(a)') 'Allocate failed for ntab in ${RName}$'
        write(ERROR_UNIT, '(a)') error_message
        call mpi_abort(MPI_COMM_WORLD, allocstat, mpierr)
      end if
#endif
      
      h5_kind = h5kind_to_type(${k1}$,H5_REAL_KIND)
      local_dims = int(ubounds,kind=hsize_t)
      if(present(empty) ) then
        if(empty) then
          local_dims(${rank}$) = 0
        end if
      end if
 
      call mpi_allgather(local_dims(${rank}$), 1, MPI_INTEGER, ntab, 1, MPI_INTEGER, comm, mpierr)

      global_dims = int(ubounds,kind=hsize_t)
      global_dims(${rank}$) = 0
  
      do iproc = 1, nproc
        global_dims(${rank}$) = global_dims(${rank}$) + ntab(iproc)
      end do
  
      if(global_dims(${rank}$) /= 0) then
        offset = 0
  
        do iproc = 1, idproc
          offset(${rank}$) = offset(${rank}$) + ntab(iproc)
        end do
  
        call h5screate_simple_f(${rank}$, global_dims, dspace_id, h5err)
        call h5dcreate_f(id, name, h5_kind, dspace_id, dset_id, h5err)
        call h5screate_simple_f(${rank}$, local_dims, mem_id, h5err)  
        call h5dget_space_f(dset_id, dspace_id, h5err)
        call h5sselect_hyperslab_f(dspace_id, H5S_SELECT_SET_F, offset, local_dims, h5err)
        call h5pcreate_f(H5P_DATASET_XFER_F, plist_id, h5err)
        if(present(indy)) then
          if(indy) then
            call h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_INDEPENDENT_F, h5err)
          else
            call h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, h5err)
          end if
        else
          call h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, h5err)
        end if
        call h5dwrite_f(dset_id, h5_kind, ptr, h5err, file_space_id = dspace_id, mem_space_id = mem_id, xfer_prp = plist_id)
        call h5dclose_f(dset_id, h5err)
        call h5sclose_f(dspace_id, h5err)
        call h5pclose_f(plist_id, h5err)
        call h5sclose_f(mem_id, h5err)
      end if
      deallocate(ntab)
    
#ifdef DEBUGHDF5
      write(ERROR_UNIT,'(a,i0)') '${RName}$ ends on process ', idproc
#endif
    end subroutine ${RName}$

  #:endfor
#:endfor

end module fhi_write_mpi_data_m